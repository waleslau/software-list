# software-list

My usual software

> 镜像了一些常用软件的镜像站
>
> - <https://mirrors.tuna.tsinghua.edu.cn/github-release/>
> - <https://mirrors.sdu.edu.cn/software/>
> - <https://mirrors.nju.edu.cn/github-release/>
>
>   ...
>
> 另：[软件源与镜像源 - Linux 银河漫游指南](https://linuxhitchhiker.github.io/THGLG/advanced/re-entry/repo-and-mirror/#%E9%95%9C%E5%83%8F%E7%AB%99%E5%88%97%E8%A1%A8)

## 启动盘制作工具

| 名称         | 链接                            |
| ------------ | ------------------------------- |
| Ventoy       | <https://www.ventoy.net/>       |
| Rufus        | <https://rufus.ie/>             |
| balenaEtcher | <https://www.balena.io/etcher/> |

_如果你的网络下载这几个工具有困难，可以试试最上面那几个镜像站_

## 常用软件

| 名称                   | 介绍                                                                                   |
| ---------------------- | -------------------------------------------------------------------------------------- |
| ~~icalingua~~          | ~~QQ 第三方客户端~~ 已删库                                                             |
| typora                 | A markdown text editor                                                                 |
| zotero                 | Tool to help collect, organize, cite, and share research source                        |
| calibre                | EBook Management Application                                                           |
| xreader                | Document viewer for documents like PDF/PostScript                                      |
| crow-translate         | A Qt GUI for Google, Yandex and Bing translators                                       |
| Qv2ray                 | Unleash Your V2Ray                                                                     |
| v2rayA                 | [一个支持全局透明代理的 V2Ray Linux 客户端](https://github.com/v2rayA/v2rayA/releases) |
| cheese                 | Webcam Booth for GNOME                                                                 |
| JetBrains Toolbox App  | [Manage your IDEs the easy way](https://www.jetbrains.com/zh-cn/toolbox-app/)          |
| brasero                | CD/DVD burning application for GNOME                                                   |
| gns3-gui & gns3-server | A graphical network simulator                                                          |

## 远程同步/下载工具

| 名称                         | 介绍                                       |
| ---------------------------- | ------------------------------------------ |
| grsync                       | rsync 的图形界面                           |
| syncthing                    | Continuous File Synchronisation            |
| FreeFileSync                 | synchronize files and folders              |
| qBittorrent-Enhanced-Edition | A BitTorrent client in Qt                  |
| rclone                       | Rsync for cloud storage                    |
| 阿里云盘小白羊版             | <https://github.com/liupan1890/aliyunpan/> |

## 系统管理

| 名称      | 介绍                                                            |
| --------- | --------------------------------------------------------------- |
| cockpit   | Web Console for Linux servers                                   |
| neofetch  | CLI system information tool written in BASH                     |
| filelight | Graphical disk usage viewer                                     |
| baobab    | Disk Usage Analyzer for Gnome                                   |
| btop      | beautiful top！                                                 |
| wireshark | A Network Traffic Analyser                                      |
| frp       | [反向代理工具，常用于内网穿透](https://github.com/fatedier/frp) |

## 视频剪辑/图片编辑

| 名称                 | 介绍                               |
| -------------------- | ---------------------------------- |
| shotcut              | Video and audio editor and creator |
| kdenlive             | Non-linear video editor            |
| simplescreenrecorder | A feature-rich screen recorder     |
| obs-studio           | A recording/broadcasting program   |
| krita                | Digital Painting Application       |
| flameshot            | 火焰截图                           |

## 影音

| 名称                | 介绍                                                                 |
| ------------------- | -------------------------------------------------------------------- |
| clementine          | 本地音乐播放器，好用！                                               |
| audacious           | 本地音乐播放器，也挺好用                                             |
| deadbeef            | 本地音乐播放器，界面很简洁，也不错                                   |
| lx-music-desktop    | [免费的在线音乐播放器](https://github.com/lyswhut/lx-music-desktop/) |
| netease-cloud-music | 网易云音乐 Linux 版                                                  |
| dragonplayer        | Multimedia Player                                                    |

## KDE

| 名称                        | 介绍                                                      |
| --------------------------- | --------------------------------------------------------- |
| krfb                        | Screen sharing using the VNC/RFB protocol                 |
| krdc                        | 远程桌面连接                                              |
| latte-dock                  | Replacement Dock for Plasma Desktops                      |
| yakuake                     | Drop-down terminal emulator based on Konsole technologies |
| kompare                     | 文件比较器                                                |
| plasma-applet-eventcalendar | Plasma 日历插件                                           |
| kronometer                  | A stopwatch application by KDE                            |
| kcharselect                 | KDE 字符选择器                                            |
| kolourpaint                 | Paint Program                                             |

## 游戏

| 名称         | 介绍                  |
| ------------ | --------------------- |
| supertuxkart | A 3D kart racing game |
| kmines       | 类扫雷游戏 by KDE     |

## 其他

| 名称     | 介绍                               |
| -------- | ---------------------------------- |
| kalgebra | Math Expression Solver and Plotter |

## 浏览器插件

-
